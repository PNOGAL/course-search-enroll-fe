import { RawYearCode } from './without-era';
import { Era, TermCode } from './termcode';

export class YearCode extends RawYearCode {
  private eras: { fall: Era; spring: Era; summer: Era };

  constructor(str: string, fall: Era, spring: Era, summer: Era) {
    super(str);
    this.eras = { fall, spring, summer };
  }

  public fall() {
    return new TermCode(super.fall(), this.eras.fall, this);
  }

  public spring() {
    return new TermCode(super.spring(), this.eras.spring, this);
  }

  public summer() {
    return new TermCode(super.summer(), this.eras.summer, this);
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { ConstantsService } from '@app/degree-planner/services/constants.service';

@Pipe({ name: 'shortSubjectDescription', pure: true })
export class ShortSubjectDescription implements PipeTransform {
  constructor(private constants: ConstantsService) {}

  transform(arg: string | { subjectCode: string }) {
    const subjectCode = typeof arg === 'string' ? arg : arg.subjectCode;
    const descriptions = this.constants.subjectDescription(subjectCode);
    return descriptions.short;
  }
}

@Pipe({ name: 'longSubjectDescription', pure: true })
export class LongSubjectDescription implements PipeTransform {
  constructor(private constants: ConstantsService) {}

  transform(arg: string | { subjectCode: string }) {
    const subjectCode = typeof arg === 'string' ? arg : arg.subjectCode;
    const descriptions = this.constants.subjectDescription(subjectCode);
    return descriptions.long;
  }
}

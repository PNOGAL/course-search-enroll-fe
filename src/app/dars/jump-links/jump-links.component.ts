import { Component } from '@angular/core';

@Component({
  selector: 'cse-dars-jump-links',
  template: `
    <a class="show-on-focus" href="/degree-planner/dars#dars-container"
      >Skip to main content</a
    >
  `,
})
export class JumpLinksComponent {}

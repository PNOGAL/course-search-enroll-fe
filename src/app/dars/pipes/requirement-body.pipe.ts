import { Pipe, PipeTransform } from '@angular/core';
import {
  Requirement,
  ContentType,
  SubRequirementCourses,
  RequirementContents,
} from '../models/audit/requirement';

type RequirementBody =
  | ContentType & { template: 'lines' }
  | SubRequirementCourses & { template: 'courses' };

@Pipe({ name: 'requirementBody', pure: true })
export class RequirementBodyPipe implements PipeTransform {
  private static reducer(
    acc: RequirementBody[],
    item: RequirementContents,
  ): RequirementBody[] {
    switch (item.contentType) {
      case 'okRequirementTitle':
      case 'noRequirementTitle':
      case 'hText':
        return acc;
      case 'okSubrequirementCourses':
      case 'noSubrequirementCourses':
        return [...acc, { ...item, template: 'courses' }];
      default:
        return [...acc, { ...item, template: 'lines' }];
    }
  }

  transform(requirement: Requirement) {
    return requirement.requirementContents.reduce(
      RequirementBodyPipe.reducer,
      [],
    );
  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'auditFormatName' })
export class AuditNamePipe implements PipeTransform {
  transform(name: string): string {
    // Regex to match commas with no spaces after
    const regex = /(,(?=\S)|:)/;
    name = name.replace(regex, ', ');

    return name;
  }
}

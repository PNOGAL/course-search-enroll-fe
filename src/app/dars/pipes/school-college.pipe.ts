import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'schoolOrCollege', pure: true })
export class SchoolOrCollegePipe implements PipeTransform {
  transform(string: string): string {
    if (string.includes(', School of')) {
      const index = string.indexOf(', School of');
      string = string.substr(0, index);
    }

    if (string.includes(', College of')) {
      const index = string.indexOf(', College of');
      string = string.substr(0, index);
    }
    return string;
  }
}

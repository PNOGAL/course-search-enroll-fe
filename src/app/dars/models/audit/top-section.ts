export interface AuditTopSection {
  nonProductionCautionTextLabel: null | string;
  nonProductionCautionText: null | string;
  serverUsedLabel: null | string;
  serverUsed: null | string;
  whatIfCautionText: string;
  degreePlannerPlanName: string;
  testDegreeProgramUsedNotice: null | string;
  programYearSection: null | {
    primaryProgram: string;
    yearInSchool: string;
    label: string;
  };
  academicPlansSection: {
    planList: null | readonly AuditPlan[];
  };
  advisorSection: {
    advisorLabel: null | string;
    advisorNames: null | readonly string[];
  };
  highSchoolUnitsSection: null | {
    highSchoolUnitsLabel: null | string;
    units: null | readonly AuditUnits[];
  };
  advancedStandingCreditsSection: null | {
    sectionLabel: string;
    advancedStandingLabels: AdvancedStandingLabels;
    advanceStandingCredits: readonly AuditAdvanceStandingCredits[];
    totals: {
      totalsLabel: string;
      degreeCreditsValue: string;
      courseCreditsValue: string;
    };
  };
  degreesSection: null | {
    label: string;
    degrees: readonly AuditDegree[];
  };
  intentToGraduate: null | string;
  admitTypeLabel: null | string;
  admitType: null | string;
  academicActions: null | string;
}

export interface AuditPlan {
  planTypeLabel: string;
  declareDate: string;
  planCodeNumber: string;
  planCodeDescription: string;
}

export interface AuditUnits {
  unitLabel: string;
  subjectUnits: null | readonly AuditSubjectUnit[];
}

export interface AuditSubjectUnit {
  subject: string;
  unitsTaken: string;
}

export interface AdvancedStandingLabels {
  dateLabel: string;
  typeLabel: string;
  degreeCreditsLabel: string;
  courseCreditsLabel: string;
}

export interface AuditAdvanceStandingCredits {
  dateValue: string;
  typeValue: string;
  degreeCreditsValue: string;
  courseCreditsValue: string;
}

export interface AuditDegree {
  awardedDate: string;
  degreeCode: string;
  major: string;
}

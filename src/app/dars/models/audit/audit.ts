import { AuditTopSection } from './top-section';
import { AuditBottomSection } from './bottom-section';
import { Requirement } from './requirement';

export interface Audit {
  header: AuditHeader;
  topSection: AuditTopSection;
  requirements: Requirement[];
  completeText: null | string;
  requirementEndnote: null | string;
  bottomSection: AuditBottomSection;
  errorText: readonly unknown[];
}

export interface AuditHeader {
  preparedLabel: string;
  preparedDate: string;
  studentCampusId: string;
  studentName: null | string;
  darsCatalogYearTermLabel: null | string;
  darsCatalogYearTerm: null | string;
  darsDegreeProgramCodeLabel: null | string;
  darsDegreeProgramCode: null | string;
  darsAlternateCatalogYearTerm1Label: null | string;
  darsAlternateCatalogYearTerm1: null | string;
  clientDefinedMessage: string;
  degreeProgramTitle1: null | string;
  degreeProgramTitle2: null | string;
}

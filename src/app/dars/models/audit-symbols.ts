export type AuditSymbol = AuditIconSymbol | AuditTextSymbol;

/**
 *  type: If the symbol should be plain text or a mat-icon.
 *  text: The original text of the symbol, will be used for find / replaces.
 *  icon: (type "icon" only) the text of the mat-icon.
 *  tooltip: The text to be displayed as the tooltip.
 *  slug: Text based identifier for the symbol. Used for createing class names.
 */
export interface AuditTextSymbol {
  type: 'text' | 'icon';
  text: string;
  tooltip?: string;
  color?: 'green' | 'red' | 'blue';
  taxonomy?:
    | 'course'
    | 'grade'
    | 'requirement'
    | 'subrequirement'
    | 'exception';
}

export interface AuditIconSymbol extends AuditTextSymbol {
  type: 'icon';
  icon: string;
}

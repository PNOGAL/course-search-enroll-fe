import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { GlobalState } from '@app/core/state';
import { MediaMatcher } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { isSidenavOpen } from '../store/selectors';
import { map } from 'rxjs/operators';
import { OpenSidenav, CloseSidenav } from '../store/actions/ui.actions';

@Component({
  selector: 'cse-degree-planner-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public mobileView: MediaQueryList;
  public isSidenavOpen$: Observable<boolean>;

  constructor(private store: Store<GlobalState>, mediaMatcher: MediaMatcher) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
  }

  ngOnInit() {
    this.isSidenavOpen$ = this.store.pipe(select(isSidenavOpen)).pipe(
      map(isOpen => {
        if (isOpen === 'defer') {
          return !this.mobileView.matches;
        }

        return isOpen;
      }),
    );
  }

  public openSidenav() {
    this.store.dispatch(new OpenSidenav());
  }

  public closeSidenav() {
    this.store.dispatch(new CloseSidenav());
  }
}

import { PreferencesResolver } from './../core/preferences.resolver';
import { ConstantsService } from '@app/degree-planner/services/constants.service';
import { DegreePlannerViewComponent } from './degree-planner-view/degree-planner-view.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: DegreePlannerViewComponent,
    resolve: { constants: ConstantsService, preferences: PreferencesResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ConstantsService, PreferencesResolver],
})
export class DegreePlannerRoutingModule {}

import { Component } from '@angular/core';

@Component({
  selector: 'cse-degree-planner-jump-links',
  template: `
    <a class="show-on-focus" href="/degree-planner/#maincontent"
      >Skip to main content</a
    >
    <a class="show-on-focus" href="/degree-planner/#utilityMenu"
      >Skip to utility menu</a
    >
  `,
})
export class JumpLinksComponent {}

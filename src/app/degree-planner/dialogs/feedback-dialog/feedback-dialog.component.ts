import { Component } from '@angular/core';

@Component({
  selector: 'cse-feedback-dialog',
  templateUrl: './feedback-dialog.component.html',
})
export class FeedbackDialogComponent {
  constructor() {}

  shareFeedback() {
    window.open(
      'https://uwmadison.co1.qualtrics.com/jfe/form/SV_cMEHiSdY5OeNFDT',
    );
  }
}

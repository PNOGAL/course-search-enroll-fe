import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core/core.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SavedForLaterContainerComponent } from './saved-for-later-container.component';
import { CourseItemComponent } from '../shared/course-item/course-item.component';

describe('SavedForLaterContainerComponent', () => {
  let component: SavedForLaterContainerComponent;
  let fixture: ComponentFixture<SavedForLaterContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, CoreModule, SharedModule, DragDropModule],
      providers: [],
      declarations: [SavedForLaterContainerComponent, CourseItemComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedForLaterContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export interface UserPreferences {
  degreePlannerGradesVisibility?: boolean;
  degreePlannerSelectedPlan?: number;
  degreePlannerHasDismissedDisclaimer?: boolean;
  degreePlannerHasDismissedIEWarning?: boolean;
  darsHasDismissedDisclaimer?: boolean;
}

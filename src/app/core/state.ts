import { DARSState } from '@app/dars/store/state';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import { UserPreferences } from './models/user-preferences';

export interface GlobalState {
  preferences: UserPreferences;
  dars: DARSState;
  degreePlanner: DegreePlannerState;
}

export const INITIAL_PREFERENCES_STATE: UserPreferences = {};
